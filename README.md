# Projeto Prático 3

Projeto de um jogo incremental para a disciplina de Desenvolvimento de Sistema WEB, utilizando PHP8, Javascript, MariaDB e Blender para os assets. Nos testes, o servidor foi rodado utilizando a ferramenta Apache2.

Neste repositótio encontram-se os códigos-fonte, bem como um vídeo de aprensentação do projeto.

![](apresentacaoBrunoEJeferson.mp4)

