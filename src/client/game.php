<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>ttt</title>
</head>
<body>
	<img style="width: 100%; height: 100%" src="http://10.0.0.103/ttt/images/continent.png" />
	<button> <img style="position: absolute; transform: scale( 1.0 ); width: 30%; height: 20%; top: 325px; left: 820px;" src="http://10.0.0.103/ttt/images/pointer.png" /> </button>

		<button id="unlock" name="test" style="position: absolute; margin-left: 1035px; margin-top: 350px;">Produce</button>
		<img style="position: absolute; transform: scale( 1.0 ); width: 30%; height: 20%; top: 325px; left: 500px;" src="http://10.0.0.103/ttt/images/lpointer.png" />
		<button style="position: absolute; top: 355px; left: 690px;"id="addworker">Add Worker</button>

		<div id="leftNav">
			<div style="background-color: transparent; display: grid; grid-row-gap: 0px;height: 50px; gap: 0px; color: #ffc;">
				<img style="top: 19px; width: 30%; height: 20%" src="http://10.0.0.103/ttt/images/coin.png" />
				<p id="money" >0</p> <!---use php to load correct value!--->
			</div>
			<div id="mtextsec">
				<div id="new"style="position: relative; top: 8px; left: -5px; font-size: 10px; color: #ffc;">0.1k/s</div>
			</div>
			<div style="background-color: transparent; display: grid; grid-row-gap: 0px;height: 50px; gap: 0px; color: #ffc;">
				<img style="top: 93px; width: 30%; height: 20%" src="http://10.0.0.103/ttt/images/helmet.png" />
				<p id="workers">0</p>
			</div>
		</div>

		<div id="intro">
			<p>Welcome to the Game! Currently you have one worker, but you can upgrade using the shop, so you can earn more money!</p>
			<button id="cont">Continue...</button>
		</div>

		<div id="nav">
			<button class="navitem" id="inventory">
				<img style="width: 100px; height: 70px; position: absolute; left: 2px;" src="http://10.0.0.103/ttt/images/market.png"/>
			</button>
			<button class="navitem" id="market">
				<img style="width: 100px; height: 70px; position: absolute; left: 2px;" src="http://10.0.0.103/ttt/images/market.png"/>
			</button>
			<button class="navitem" id="stats">
				<img style="width: 100px; height: 70px; position: absolute; left: 2px;" src="http://10.0.0.103/ttt/images/market.png"/>
			</button>
			<button class="navitem" id="map">
				<img style="width: 100px; height: 70px; position: absolute; left: 2px;" src="http://10.0.0.103/ttt/images/market.png"/>
			</button>
			<button class="navitem" id="rankings">
				<img style="width: 140px; height: 80px; position: absolute; left: 0; right: 0; transform: translateX(-15%);" src="http://10.0.0.103/ttt/images/medal.png"/>
			</button>
		</div>

		<iframe id="container" src="">
		</iframe>

<script src="functions.js" type="text/javascript"></script>
<script type="module">
		var cont = document.querySelector("#cont")
		var intro = document.querySelector("#intro")

		cont.onclick = () => {
			intro.style="display: none;"
		}

		var inv = document.querySelector("#inventory")
		var frame = document.querySelector("#container")

		inv.onclick = () => 
		{
			if( frame.style.display == "none" )
			{ 
				frame.src = 'http://10.0.0.103/ttt/src/client/inventory.php'
				frame.style.display = "block" 
			}
			else
			{
				frame.style.display = "none"
			}
		}
		
		var market = document.querySelector( "#market" )

		market.onclick = () =>
		{
			if( frame.style.display == "none" )
			{ 
				frame.src = 'http://10.0.0.103/ttt/src/client/market.php'
				frame.style.display = "block" 
			}
			else
			{
				frame.style.display = "none"
			}
		}
		stats.onclick = () =>
		{
			if( frame.style.display == "none" )
			{ 
				frame.src = 'http://10.0.0.103/ttt/src/client/stats.php'
				frame.style.display = "block" 
			}
			else
			{
				frame.style.display = "none"
			}
		}
		map.onclick = () =>
		{
			if( frame.style.display == "none" )
			{ 
				frame.src = 'http://10.0.0.103/ttt/src/client/map.php'
				frame.style.display = "block" 
			}
			else
			{
				frame.style.display = "none"
			}
		}
		
		rankings.onclick = () =>
		{
			if( frame.style.display == "none" )
			{ 
				frame.src = 'http://10.0.0.103/ttt/src/client/rankings.php'
				frame.style.display = "block" 
			}
			else
			{
				frame.style.display = "none"
			}
		}

		function callbackmoney( res )
		{
				console.log( res )
				money.innerText = parseFloat(res.money) + (parseFloat( res.money ) * parseFloat( res.modifier ) );
		}

		function callback( res )
		{
				console.log( res )
				workers.innerText = parseFloat(res.workers);
		}

		unlock.onclick = () =>
		{
			const params = { money: "money" }
			query( "Money.php", JSON.stringify(params), callbackmoney)
			//money.innerText = parseFloat( money.innerText ) + parseFloat( 1 )
		}

		addworker.onclick = () =>
		{
			const params = { worker: "worker" }
			query( "Worker.php", JSON.stringify(params), callback)
			//workers.innerText = parseFloat( workers.innerText ) + parseFloat( 1 )
		}
</script>


</body>
<style>

#leftNav{
	display: grid;
	grid-template-columns: auto;
	position: absolute;
	background-color: transparent;
	width:  150px;
	height: 150px;
	left: 2.5%;
	top: 2.5%;
	text-align: center;
	padding: 0px;
	grid-row-gap: 0px;
}

#money{
	background-color: rgba( 1,1,1, 0.1 );
	border-radius: 5px;
}

#workers{
	background-color: rgba( 1,1,1, 0.1 );
	border-radius: 5px;
}

#mtextsec{
	position: absolute;
	top:  39px;
	left: 35px;
	width: 100px;
	text-align: center;
	justify-contents: center;
}

#mtextsec::before{
	display: none;
	position: absolute;
	content: "";
	width: 70px;
	border: 8px solid orange;
	border-top: 8px solid orange;

}

#mtextsec::after{
	position: absolute;
	content: "";
	top: 5px;
	left: -5px;
	width: 76px;
	border: 9px solid transparent;
	border-top: 18px solid rgba(1,1,1,0.1);

}

#loadText{ position: absolute; }

iframe{
	display: none;
	position: absolute;
	background-color: rgba( 255,255,255, 0.7 );	
	left: 20%;
	top: 5%;
	width: 1200px;
	height: 900px;
	overflow-x: hidden;
	border: 1px solid rgba( 100, 100, 100, 0.1 );
}
	
#nav{
	position: absolute;
	left: 93%;
	top: 30%;
	padding: 0px;
	background-color: transparent;
	
}
.navitem{
	width: 100px;
	height: 95px;
	border: none;
	background-color: transparent;
	display: grid;
	align-items: center;
	padding: 10px;
	border-radius: 5px;
}
.navitem:hover{
	transform: scale( 1.1 );
	border: none;
	background-color: rgba( 2,2,2,0.1 );
}
#c {
	width: 100%;  
	height: 100%;
	display: block;
}

#intro {
	position: absolute;
	font-size: 20px;
	/*left: 40%;
	top: 500px;*/
	margin-left: auto;
	margin-right: auto;
	left: 0;
	right: 0;
	top: 50%;
	transform: translateY(-50%);
	width: 500px;
	background-color: rgba( 2,2,2,0.5 );
	border-radius: 5px;
	align-items: center;
	justify-content: center;
	vertical-align: middle;
	box-shadow: 0px 2px 2px 0px black;
	color: #ffc;
}
p{ font-size: 20px;vertical-align: middle; }

#worker, #load{
	position: absolute;  
	left: 50px;             
	top: 90px;
	color: #222222;
	background-color: transparent;
	width: 30px;
	height: 10px;
	text-align: center;
	justify-content: center;
	border: none;
}

#load{ top: 140px; }
#test:hover{ background-color: #555555; }

img{
	position: absolute;
	background-color: transparent;
	border: none;
}

#cont{
	font-size: 20px;
	display: relative;
	background-color: transparent;
	border: none;
	color: #ffc;
}

#label {
	position: absolute;  
	left: 50px;             
	top: 50px;
	color: #222222;
	background-color: transparent;
	border-radius: 15em;
	width: 90px;
	height: 25px;
	text-align: center;
	justify-content: center;
}
#unlock{
	color: #ffe;
	height: 25px;
	border: none;
	background-color: rgba( 1,1,1,0.5 );
	border-radius: 5px;
}

#addworker{

	color: #ffe;
	height: 25px;
	border: none;
	background-color: rgba( 1,1,1,0.5 );
	border-radius: 5px;
}

#unlock:hover, #addworker:hover{ background-color: #ffbc00; }

</style>
</html>

