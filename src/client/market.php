<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Inventory</title>
</head>
<body>
	<div id='container'>
		<div id="show">
			<img id="shower" src="http://10.0.0.103/ttt/images/dirt2.png"/>
		</div>

		<div class="about">
			<!---<h1 id="title" = style="width: 100px;">Dirt</h1>!--->
			<h1 id="title"  style="width: 100px;">Dirt</h1>
			<p id="description">Base item of the game. You may cleanse it to obtain raw minerals, 
				sell it on the market, or trade for more land on the future.</p>
		</div>

		<div id="action">
			<div id="money"><p style="position: relative; right: -50px;">0</p><img style="position: relative; width: 90px; left: -50px; top: -55.5px;"src="http://10.0.0.103/ttt/images/coin.png"></img></div>
			<br></br>
			<button style="margin-left: 12.5%;"id="sell">Sell</button>
			<button id="buy" style="margin-left: 0%;">Buy</button>
			<br></br>
			<button style="margin-left: 16%; width: 30px; height: 28px;"id="minus">-</button>
			<input id="quantity" placeholder="1"></input>
			<button style="width: 30px; height: 28px;"id="plus">+</button>
			<!---<button .class="baction" id="sell" >Sell</button>!--->
			<p id="info" style="width: 170px;">dsadsa</p>
			<div class="about">
				<button style="display: none; " id="cancel" >Cancel</button>
				<button style="display: none; margin-right: 15%;" id="confirm">Confirm</button>
			</div>
		</div>
	</div>

	<br></br>

	<div class="sbcontainer" id="sellContainer">
		<div class="item" id="dirt">
			<img id="dirtimage" style="" src="http://10.0.0.103/ttt/images/dirt2.png"/>
			<p id="p-dirt">0</p>
		</div>
		<div class="item" id="gold">
			<img id="rawCrystal" style="" src="http://10.0.0.103/ttt/images/out2.png"/>
			<p id="p-crystal">0</p>
		</div>
		<div class="item" id="emerald">
			<img id="rawCrystal" style="" src="http://10.0.0.103/ttt/images/out.png"/>
			<p id="p-crystal">0</p>
		</div>
		<div class="item" id="amethyst">
			<img id="rawCrystal" style="" src="http://10.0.0.103/ttt/images/out3.png"/>
			<p id="p-crystal">0</p>
		</div>
		<div class="item" id="ruby">
			<img id="rawCrystal" style="" src="http://10.0.0.103/ttt/images/out4.png"/>
			<p id="p-crystal">0</p>
		</div>
		<div class="item" id="ambar">
			<img id="rawCrystal" style="" src="http://10.0.0.103/ttt/images/out1.png"/>
			<p id="p-crystal">0</p>
		</div>
	</div>

	<div class="sbcontainer" id="buy" style="display: none;">
		<div class="item" id="dirt">
			<img id="dirtimage" style="" src="http://10.0.0.103/ttt/images/dirt2.png"/>
			<p id="p-dirt">0</p>
		</div>
		<div class="item" id="gold">
			<img id="rawCrystal" style="" src="http://10.0.0.103/ttt/images/out2.png"/>
			<p id="p-crystal">0</p>
		</div>
		<div class="item" id="emerald">
			<img id="rawCrystal" style="" src="http://10.0.0.103/ttt/images/out.png"/>
			<p id="p-crystal">0</p>
		</div>
		<div class="item" id="amethyst">
			<img id="rawCrystal" style="" src="http://10.0.0.103/ttt/images/out3.png"/>
			<p id="p-crystal">0</p>
		</div>
		<div class="item" id="ruby">
			<img id="rawCrystal" style="" src="http://10.0.0.103/ttt/images/out4.png"/>
			<p id="p-crystal">0</p>
		</div>
		<div class="item" id="ambar">
			<img id="rawCrystal" style="" src="http://10.0.0.103/ttt/images/out1.png"/>
			<p id="p-crystal">0</p>
		</div>
	</div>

</body>
<script>
	var cancel      = document.querySelector( '#cancel'      )
	var confirm     = document.querySelector( '#confirm'     )
	var sell        = document.querySelector( '#sell'        )
	var buy         = document.querySelector( '#buy'         )
	var text        = document.querySelector( '#text'        )
	var shower      = document.querySelector( '#shower'      )
	var item1       = document.querySelector( '#dirt'        )
	var gold        = document.querySelector( '#gold'        )
	var emerald     = document.querySelector( '#emerald'     )
	var amethyst    = document.querySelector( '#amethyst'    )
	var ruby        = document.querySelector( '#ruby'        )
	var ambar       = document.querySelector( '#ambar'       )
	var info        = document.querySelector( '#info'        )
	var title       = document.querySelector( '#title'       )
	var description = document.querySelector( '#description' )
	var quantity    = document.querySelector( '#quantity'    )
	var minus       = document.querySelector( '#minus'       )
	var plus        = document.querySelector( '#plus'        )

	var showerOn = 0, lastClick = 0

	quantity.value = 1

	sell.onclick = () =>
	{
		var value = 1000
		info.innerText = "Do you really want to sell these items for: $" + parseFloat( value )
		info.style = "display: block; width: 290px;"
		cancel.style.display="inline-grid"
		confirm.style.display="inline-grid"
	}
	cancel.onclick = () =>
	{
		cancel.style.display  = "none"
		confirm.style.display = "none"
		text.innerText = "Init"
		lastClick = 0
		showerOn  = 0
	}
	dirt.onclick = () =>
	{
		shower.src            = "http://10.0.0.103/ttt/images/dirt2.png"
		shower.style          = "transform: scale( 0.55 ); left: 25px; top: -0px;"
		info.innerText        = "Cleanse this item to get raw ores."
		title.innerText       = "Dirt"
		description.innerText = "Base item of the game. You may cleanse it to obtain raw minerals, sell it on the market, or trade for more land on the future."
	}
	gold.onclick = () =>
	{
		shower.src            = "http://10.0.0.103/ttt/images/out2.png"
		shower.style          = "transform: scale( 0.55 ); margin-left: 7%; margin-top: -36%;"
		info.innerText        = "Smelt this item to get gold bars."
		title.innerText       = "Raw Gold"
		description.innerText = "May be smelted to generate pure gold."
	}
	emerald.onclick = () =>
	{
		shower.src="http://10.0.0.103/ttt/images/out.png"
		shower.style="transform: scale( 0.55 ); margin-left: 7%; margin-top: -36%;"
		info.innerText        = "Smelt this item to create emerald gems."
		title.innerText       = "Raw Emerald"
		description.innerText = "May be smelted to create a valuable emerald gem."
	}
	amethyst.onclick = () =>
	{
		shower.src="http://10.0.0.103/ttt/images/out3.png"
		shower.style="transform: scale( 0.55 ); margin-left: 7%; margin-top: -36%;"
		info.innerText        = "Smelt this item to create amethyst gems."
		title.innerText       = "Raw Amethyst"
		description.innerText = "May be smelted to create a valuable amethyst gem."
	}
	ruby.onclick = () =>
	{
		shower.src="http://10.0.0.103/ttt/images/out4.png"
		shower.style="transform: scale( 0.55 ); margin-left: 7%; margin-top: -36%;"
		info.innerText        = "Smelt this item to create ruby gems."
		title.innerText       = "Raw Ruby"
		description.innerText = "May be smelted to create a valuable ruby gem."
	}
	ambar.onclick = () =>
	{
		shower.src="http://10.0.0.103/ttt/images/out1.png"
		shower.style="transform: scale( 0.55 ); margin-left: 7%; margin-top: -36%;"
		info.innerText        = "Smelt this item to create ambar gems."
		title.innerText       = "Raw Ambar"
		description.innerText = "May be smelted to create a valuable ambar gem."
	}

	plus.onclick = () => 
	{
		//if( parseFloat( quantity.value ) + parseFloat( 1 ) > itemmax ) quantity.value = itemmax
		quantity.value = parseFloat( quantity.value ) + parseFloat( 1 )
	}
	minus.onclick = () => 
	{
		if( parseFloat( quantity.value ) - parseFloat( 1 ) > 0 )
			quantity.value = parseFloat( quantity.value ) - parseFloat( 1 )
	}

</script>
<style>
body{ overflow: hidden; background-color: rgba( 1,1,1,0.8 ); }
#show{
	background-color: rgba( 1,1,1,0.5 );
	width:  280px;
	height: 280px;
	border-radius: 5px;
	margin-left: 20px;
	margin-top: 15px;
}

#shower{
	transform: scale( 0.55 );
	margin-left: 7%;
	top:  0px;
}

.about{
	text-align: center;
	align-items: center;
	justify-content: center;
	width: 350px;
}

#clean, #smelt {
	margin-left: 20%;
}
button{
	width:  75px;
	height: 25px;
	border: none;
	background-color: rgba( 1,1,1,0.5 );
	border-radius: 5px;
}

#cancel{ margin-left: -37.5px; background-color: red; }
#confirm{ background-color: green; }
#cancel:hover{ background-color: darkred; }
#confirm:hover{ background-color: darkgreen; }

button:hover{ background-color: #ffbc00; }

#action{
	background-color: rgba( 1,1,1,0.0 );
	margin-right: -200px;
}

#p-dirt{
	position: relative;
	left: 150px;
	top: -170px;
	font-size: 21px;
}

#p-crystal{
	position: relative;
	left: 149px;
	top: -370px;
	font-size: 21px;
}

#dirtimage
{
	transform: scale(0.35);
	position: relative;
	left: -30px;
	top: -55px;
}

#rawCrystal
{
	transform: scale(0.25);
	position: relative;
	left: -35px;
	top: -150px;
}

.item{
	background-color: rgba( 1,1,1,0.5 );
	width:  175px;
	height: 175px;
	border-radius: 5px;
}

.item:hover{
	transform: scale(1.1);
}

.sbcontainer{
	display: grid;
	grid-template-columns: auto auto auto auto auto auto;
	row-gap: 10px;
	margin-left: 20px;
	margin-top: 5px;
}

#container{
	display: grid;
	grid-template-columns: auto auto auto;
	border-radius: 5px;
	border: none;
	background-color: rgba( 1,1,1,0.0 );
}

#quantity{
	border-radius: 5px;
	border: none;
	height: 25px;
	background-color: rgba( 100, 160, 120, 0.5 );
	text-align: center;
	width: 35px;
	color: white;"
}

#money{
	background-color: rgba( 1,1,1,0.5 );
	text-align: center;
	width: 150px;
	height: 30px;
	border-radius: 5px;
	vertical-align: center;
	line-height: 30px;
	margin-left: 135px;
}

*{ color: #ffe; }
</style>
</html>

