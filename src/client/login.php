<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>ttt Login</title>
</head>
<body background="http://localhost:8081/login.gif">
	<div class="container">
		<!---<div class="inner">
			<div class="front">
				<h2>Welcome to TTT</h2>
			</div>
			<div class="back">
				<p>Identify Yourself</p>
				<input id="login" placeholder="login" />
				<span class="line"></span>
				<input id="pass" type="password" placeholder="pass" />
				<button id="confirm">Confirm</button>
			</div>
		</div>!--->

				<p>Identify Yourself</p>
				<input id="login" placeholder="login" />
				<span class="line"></span>
				<input id="pass" type="password" placeholder="pass" />
				<button id="confirm">Confirm</button>
	</div>
</body>
<script>
	var confirm = document.querySelector( '#confirm' )

	confirm.onclick = () => {
		window.open( 'http://localhost:8081/game.html', "_self" )
	}
</script>
<style>
body{
	width:  100%;
	height: 100%;
}
.container{
	border: none;
	border-radius: 5px;
	display: grid;
	align-items: center;
	margin-left: 70%;
	width: 300px;
	height: 350px;
	background-color: rgba( 90,90,90,0.2 );
	justify-content: center;
	text-align: center;
	margin-top: 250px;
	box-shadow: 0px 2px 2px 0px black;
	color: #ffc;

	/*perspective: 1000px;
	transition: transform 0.6s;*/
}

/*
.inner{
position: relative;
display: grid;
width:100%;
height: 100%;
text-align: center;
transform-style: preserve-3d;
}

.container:hover ,.inner{
	transform: rotateY(180deg);
}

.front, .back{
position: absolute;
width: 100%;
height: 100%;
-webkit-backface-visibility: hidden;
backface-visibility: hidden;
}

.back{ transform: rotateY(180deg); }
*/
input, p, button{
	color: #ffc;
	width: 150px;
	border: none;
	background-color: transparent;
}

input{ height: 40px; transition: all 0.4s ease-in;}
input:focus{
	background-color: rgba( 1,1,1,0.7 );
}
button{
	border: 1px solid white;
	border-radius: 5px;
	width: 100px;
	height: 40px;
	margin-left: auto;
	margin-right: auto;
}

button:hover{
	background-color: green;
}

</style>
</html>

