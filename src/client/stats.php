<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>TTT Statistics</title>
</head>
<body>
	<h1>TTT Statistics</h1>

	<div id="container" >
		<div class="stat" >Money
			<div style="display: grid; grid-template-columns: auto auto;">
				<p>Per second:</p>
				<p>0</p>

				<p>All Time:</p>
				<p>0</p>
			</div>
		</div>
		<div class="statalt">Workers
			<div style="display: grid; grid-template-columns: auto auto;">
				<p>Total: </p>
				<p>0</p>

				<p>Allocated: </p>
				<p>0</p>
			</div>
		</div>
		<div class="statalt">Total Dirt
			<div style="display: grid; grid-template-columns: auto auto;">
				<p>Total: </p>
				<p>0</p>

				<p>Allocated: </p>
				<p>0</p>
			</div>
		</div>
		<div class="stat">Dirt Cleansed
			<div style="display: grid; grid-template-columns: auto auto;">
				<p>Total: </p>
				<p>0</p>

				<p>Allocated: </p>
				<p>0</p>
			</div>
		</div>
		<div class="stat">Lands
			<div style="display: grid; grid-template-columns: auto auto;">
				<p>Total: </p>
				<p>0</p>

				<p>Allocated: </p>
				<p>0</p>
			</div>
		</div>
		<div class="statalt">Tech
			<div style="display: grid; grid-template-columns: auto auto;">
				<p>Total: </p>
				<p>0</p>

				<p>Allocated: </p>
				<p>0</p>
			</div>
		</div>
	</div>

</body>
<style>
#container{
	position: absolute;
	display: grid;
	grid-template-columns: auto auto;
	grid-gap: 20px;
	grid-row-gap: 200px;
	left: 0;
	right: 0;
	margin-left: auto;
	margin-right: auto;
	width: 85%;
	margin-top: 1%;
	
}

h1{
	width: 300px;
	text-align: center;
	border-radius: 10px;
	margin-left: auto;
	margin-right: auto;
}
.stat{
	background-color: rgba( 000, 030, 255, 0.2 );
	height: 30px;
	text-align: center;
	border-radius: 15px;
	line-height: 30px;
	width: 500px;
}

.statalt{
	background-color: rgba( 0,255,030, 0.3 );
	height: 30px;
	text-align: center;
	border-radius: 15px;
	line-height: 30px;
	width: 500px;
}
p{
	background-color: rgba( 255,255,255, 0.3 );
	border-radius: 5px;
	width: 100px;
	margin-left: auto;
	margin-right: auto;
}
.p1{ 
	float: left;
	margin-top: 7%;
	margin-left: 2%;
}
.p2{ 
	float: right;
	margin-top: 7%;
	margin-right: 2%;
}
body{
	background-color: #282828;
}
*{color: #ffe;}
</style>
</html>

